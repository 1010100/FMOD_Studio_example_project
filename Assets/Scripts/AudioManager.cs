﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class AudioManager : MonoBehaviour
{

    private static AudioManager _instance = null;
    public static AudioManager Instance
    {
        get
        {
            return _instance;
        }

        set
        {
            _instance = value;
        }
    }

    #region FMOD References 
    //The reference strings are set this way for easy of viewing in this example.
    [FMODUnity.EventRef]
    public string
        _ExampleEventRef,
        _foodsteps,
        _doorknob,
        _lightswitch,
        _command,
        _basicMusic,
        _complexMusic,
        _basicEngine;
    #endregion

    #region FMOD Event Instances
    //The Event instances are set this way for ease of viewing as well.
    public FMOD.Studio.EventInstance
        exampleEventInstance,
        foodsteps,
        doorknob,
        lightswitch,
        command,
        basicMusic,
        complexMusic,
        basicEngine;
    #endregion

    //Stop Mode allows you to stop the audio either immediately or to allow it to fade out
    FMOD.Studio.STOP_MODE _stopMode;


    public void Awake()
    {
        #region Duplicate instance check
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
            return;
        }
        #endregion
    }

    public void FMODStartup()
    {
        exampleEventInstance = FMODUnity.RuntimeManager.CreateInstance(_ExampleEventRef);
    }

    // Use this for initialization
    void Start()
    {
        //Sets the default stopmode state
        _stopMode = FMOD.Studio.STOP_MODE.IMMEDIATE;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ExamplePlayAUdio()
    {
        exampleEventInstance.start();
    }

    public void ExampleStopAudio()
    {
        exampleEventInstance.stop(_stopMode);
        
        //If you do not want to use or store a variable for it, you can pass the stop mode straight through the function as well
        //exampleEventInstance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
    }
}
